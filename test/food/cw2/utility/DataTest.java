/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alan
 */
public class DataTest {
    
    private Data data;
    
    public DataTest() {
        data = new Data();
    }
    /**
     * Test of getStockManager method, of class Data.
     * StockManager has a Singleton design pattern. This test tests that the instance
     * returned by the Data reference is the same as the one returned by StockManager's
     * getInstace method.
     */
    @Test
    public void testGetStockManager() {
        StockManager expResult = StockManager.getInstance();
        StockManager result = data.getStockManager();
        assertEquals(expResult, result);
    }

 // ---------- Tests of GetJobs method ----------
    
    @Test
    public void testGetJobs() {
        // Test the number of job instances of the ArrayList.
       int result = data.getJobs().size();
       int expectedResult = 10;
       
       assertEquals(expectedResult, result);
    }
    
    @Test
    public void testGetJobs2(){
        // Test that job numbers are generated correctly.
        int [] numberHolder = new int[10];
        int [] numbersArray = new int[]{1,2,3,4,5,6,7,8,9,10};
        boolean expectedResult = true;
        boolean result = true;
        
        // Store the job number values.
        for(int i = 0; i<data.getJobs().size(); i++)
        {
          numberHolder[i] = data.getJobs().get(i).getNumber();          
        } 
        
        // Compare the values with the expected values, if a value does not match result is false.
        for(int i = 0; i<data.getJobs().size(); i++)
        {
            if(numberHolder[i] != numbersArray[i])
            { 
                result = false;
                System.out.println(numberHolder[i]);
            }
        }       
        assertEquals(expectedResult, result);        
    }
    
   // ------- Tests of GetJob method --------
    
    @Test
    public void testGetJob() {
        // Test that the expected job is returned, verified by its number.
        Integer result = data.getJob(10).getNumber();
        Integer expectedResult = 10;
        assertEquals(expectedResult, result);
        
        result = data.getJob(1).getNumber();
        expectedResult = 1;
        assertEquals(expectedResult, result);

        if(data.getJob(11) == null)
        {
         result = null;
        }
        expectedResult = null;
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testGetJob2(){
        // Test that the Job type of a specific job matches the expected job type.
        JobType expectedResult =  JobType.TYRE;
        JobType result = data.getJob(1).getJobType();
        assertEquals(expectedResult, result);
    }

    // --------- Test addJob method ----------
    @Test
    public void testAddJob() {
        
      Data data = new Data();
      JobNumberGenerator jng = data.GetJobNumberGenerator();
      
      data.addJob("Jason", "83399M", new Ford(), new Engine());
      
        // Test that the job list grows by a single value when a new job is added.
        int result = data.getJobs().size();
        int expectedResult = 11;
        
        assertEquals(expectedResult, result); 
    }
    
    @Test
    public void testAddJob2() {
        
      Data data = new Data();
      
       data.addJob("Jason", "83399M", new Ford(), new Engine());
       List<Job> jobs = data.getJobs();
        // Test that the job type of the added job match with the expected value.
        JobType result = jobs.get(jobs.size()-1).getJobType();
        JobType expectedResult = JobType.ENGINE;
        
        assertEquals(expectedResult, result); 
    }
    
    public void testAddJob3(){
        
        Data data = new Data();
      data.addJob("Jason", "83399M", new Ford(), new Engine());
       List<Job> jobs = data.getJobs();
        // Test that the job status of the added job match with the expected value.
        JobStatus result = jobs.get(jobs.size()-1).getStatus();
        JobStatus expectedResult = JobStatus.PENDING;
        
        assertEquals(expectedResult, result); 
    }
    
     public void testAddJob4(){
        
        Data data = new Data();
     data.addJob("Jason", "83399M", new Ford(), new Engine());
       List<Job> jobs = data.getJobs();
        // Test that the job skill required for the added job match with the expected value.
        Skill result = jobs.get(jobs.size()-1).skillRequired();
        Skill expectedResult = Skill.MASTER;
        
        assertEquals(expectedResult, result); 
    }
    
  
     // --------- GetCost tests ----------
    @Test
    public void testGetCost() {
 
        // Test that the cost of the part and vehicle combination match the expected result.
        BigDecimal result = data.getCost("Rover", "Tyre");
        BigDecimal expectedResult = BigDecimal.valueOf(100);
        assertEquals(expectedResult, result);
        
    }
    
     @Test
    public void testGetCost2() {
 
        // Test that null is returned if an invalid combination is passed.
        BigDecimal result = data.getCost("Fiat", "Tyre");
        if(data.getCost("Fiat", "Tyre") == null)
        {
            result = null;
        }
        BigDecimal expectedResult = null;
        assertEquals(expectedResult, result);   
    }

 //---------- Test GetAvailable Mechanics method -----------
    @Test
    public void testGetAvailableMechanics() {

        Data data = new Data();
        // Test that the number of mechanics matches the expected result.
        int expectedResult = 9;
        int result = data.getAvailableMechanics().size();
        assertEquals(expectedResult, result);    
    }
    
    @Test
    public void testGetAvailableMechanics2() {

        Data data = new Data();
        // Test that the selected mechainc name matches the expected result.
        String expectedResult = "Mark";
        String result = data.getAvailableMechanics().get(2).getName();
        assertEquals(expectedResult, result);    
    }

//---------- Test takePart method ----------
    @Test
    public void testTakePart() {
    Data data = new Data();
    data.takePart("Ford", "Tyre");
    // Test that a part item is removed from the quantity of the defined part.
    int result = StockManager.getInstance().getNoOfPartsByType("Tyre", "Ford");
    int expectedResult = 9; 
    }

// ---------- Test setJobOngoing method ----------
    @Test
    public void testSetJobOngoing() {
      
        Data data = new Data();
        Job job = data.getJob(1);
        data.setJobOngoing(1);
        
        JobStatus result = job.getStatus();
        JobStatus expectedResult = JobStatus.ONGOING;
        
        assertEquals(expectedResult, result);
    }

    // ---------- Test getMechaincByID method ----------
    @Test
    public void testGetMechanicByID() {
        
     Data data = new Data();
     Mechanic mechanic = data.getMechanicByID("12348");
     // Test that name matched to the name of the one created in the data instance.
     String result = mechanic.getName();
     String expectedResult = "Ryan";
     
     assertEquals(expectedResult, result);
    }

    // ---------- Test getGetOngoingJobs method ----------
    @Test
    public void testGetOngoingJobs() {
     
        Data data = new Data();
        
        int result = data.getOngoingJobs().size();
        int expectedResult = 0;
        // All jobs are by default set to Pending, thus the expected result is 0.
        assertEquals(expectedResult, result);
        
        // A Job is set to Ongoing, thus the expected result is 1.
        data.getJob(1).setStatus(JobStatus.ONGOING);
        result = data.getOngoingJobs().size();
        expectedResult = 1;
        
        assertEquals(expectedResult, result);
    }

    // ----------Test setComplete method ----------
    @Test
    public void testSetComplete() {
        
        Data data = new Data();
        
        List<Job> jobs = data.getJobs(); // By default all jobs are set to pending.
        
        // Set 5 jobs to complete from a total of 10.
        data.setComplete(1);
        data.setComplete(2);
        data.setComplete(3);
        data.setComplete(4);
        data.setComplete(5);
        
        int result = 0;
        int expectedResult = 5;
        
        // Jobs completed should be 5.
        for(Job j : jobs)
        {
            if(j.getStatus() == JobStatus.COMPLETE)
            {
                result++;
            }
        }
        assertEquals(expectedResult, result);
    }
    
}
