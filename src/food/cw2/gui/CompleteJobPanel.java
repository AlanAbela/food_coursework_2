/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Alan
 */
public class CompleteJobPanel extends JPanel{
    
    private JLabel lblTitle;
    private JLabel lblJobNo;
    private JLabel lblVehicleType;
    private JLabel lblPartType;
    private JLabel lblCustomerID;
    private JButton btnSearch;
    private JTextField txtSearch;
    private Main main;
    
    public CompleteJobPanel(Main main)
    {
        this.main = main;
        
        lblTitle = new JLabel("Job Search");
        lblJobNo = new JLabel();
        lblVehicleType = new JLabel();
        lblPartType = new JLabel();
        lblCustomerID = new JLabel();
        btnSearch = new JButton("Search");
        txtSearch = new JTextField(10);
        
        btnSearch.addActionListener(new ButtonListener());
        
        createLayout();
    }

    private void createLayout() {
        
        this.setLayout(new BorderLayout());
        
        // Setting top panel.
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new GridLayout(2,1));
        topPanel.add(lblTitle);
        
        JPanel searchPanel = new JPanel();
        searchPanel.setBorder(OuterBorder.styledBorder());
        searchPanel.setLayout(new FlowLayout());
        searchPanel.add(btnSearch);
        searchPanel.add(txtSearch);
        topPanel.add(searchPanel);
        
        this.add(topPanel, BorderLayout.NORTH);
        
        // Setting Middle Panel.
        JPanel midPanel = new JPanel();
        midPanel.setBorder(OuterBorder.styledBorder());
        midPanel.setLayout(new GridLayout(4,2));
        midPanel.add(new JLabel("Customer ID: "));
        midPanel.add(lblCustomerID);
        midPanel.add(new JLabel("Job No: "));
        midPanel.add(lblJobNo);
        midPanel.add(new JLabel("Vehicle Type: "));
        midPanel.add(lblVehicleType);
        midPanel.add(new JLabel("Part Type: "));
        midPanel.add(lblPartType);
        
        
        this.add(midPanel, BorderLayout.CENTER);       
   
    }
    
    public void clear()
    {
        lblJobNo.setText("");
        lblVehicleType.setText("");
        lblPartType.setText("");
        lblCustomerID.setText("");
        txtSearch.setText("");
    }
    
    private class ButtonListener implements ActionListener
       {

        @Override
        public void actionPerformed(ActionEvent e) {
                 
            try
            {
                Integer jobNo = Integer.valueOf(txtSearch.getText());
                
                lblJobNo.setText(String.valueOf(main.getCompleteJob(jobNo).getNumber()));
                lblVehicleType.setText(main.getCompleteJob(jobNo).getCustomer().getRepair().getVehicleType().toString());
                lblPartType.setText(main.getCompleteJob(jobNo).getCustomer().getRepair().getPartType().toString());
                lblCustomerID.setText(main.getCompleteJob(jobNo).getCustomer().getID());
                  
            }
            catch(Exception ex)
            {
                JOptionPane.showMessageDialog(CompleteJobPanel.this, "Invalid job number.", "Error Message", JOptionPane.ERROR_MESSAGE);
                clear();
            }
            
           
            
        }
        
    }
}
