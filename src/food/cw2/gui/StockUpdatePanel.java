/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import food.cw2.utility.Part;

/**
 *
 * @author Alan
 */
public class StockUpdatePanel extends JPanel{
    
    private JLabel lblTitle;
    private JList listParts;
    private DefaultListModel model;
    private JLabel lblPartNo;
    private JLabel lblType;
    private JLabel lblVehicle;
    private JLabel lblQuantity;
    private JTextField txtQuantity;
    private JLabel lblManufaturer;
    private JButton addButton;
    private Main main;
    private JScrollPane scroll;
    
    public StockUpdatePanel(Main main)
    {
        this.main = main;
        
        lblTitle = new JLabel("Stock View");
        model = new DefaultListModel();
        listParts = new JList(model);
        scroll = new JScrollPane(listParts);
        lblPartNo = new JLabel();
        lblType = new JLabel();
        lblVehicle = new JLabel();
        lblQuantity = new JLabel("Add quantity: ");
        txtQuantity = new JTextField(10);
        lblManufaturer = new JLabel();
        addButton = new JButton("Add");     
        
        setLayout();
        
        populatePartsList();
        
        listParts.addListSelectionListener(new SelectionListener());
        addButton.addActionListener(new ButtonListener());
    }
    
    // Create layout.
    private void setLayout()
    {
        this.setLayout(new GridLayout(2,1));
        this.setBorder(OuterBorder.styledBorder());
        
        // Set upper part //
        JPanel upperPanel = new JPanel();
        upperPanel.setBorder(OuterBorder.styledBorder());
        upperPanel.setLayout(new BorderLayout());
        upperPanel.add(lblTitle, BorderLayout.NORTH);
        upperPanel.add(scroll, BorderLayout.CENTER);
        this.add(upperPanel);
        
        // Set lower part //
        JPanel lowerPanel = new JPanel();
        lowerPanel.setBorder(OuterBorder.styledBorder());
        lowerPanel.setLayout(new GridLayout(6,2));
        lowerPanel.add(new JLabel("Part no: "));
        lowerPanel.add(lblPartNo);
        lowerPanel.add(new JLabel("Part type: "));
        lowerPanel.add(lblType);
        lowerPanel.add(new JLabel("Vehicle type: "));
        lowerPanel.add(lblVehicle);
        lowerPanel.add(lblQuantity);
        lowerPanel.add(txtQuantity);
        lowerPanel.add(new JLabel("Manufacturer: "));
        lowerPanel.add(lblManufaturer);
        lowerPanel.add(addButton);
        this.add(lowerPanel);     
    }
    
    // Update part list.
    public void populatePartsList()
    {
         model.clear();
        for(Part p : main.getParts())
        {
            model.addElement(p);
            
        }
    }
    
    
    // Implement ListSelectionLIstener for the JList.
    private class SelectionListener implements ListSelectionListener
    {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            
            if(e.getValueIsAdjusting())
            {
           Part p = (Part)listParts.getSelectedValue();
           
           lblPartNo.setText(p.getPartNo());
           lblType.setText(p.getPartType().getType());
           lblVehicle.setText(p.getVehicleType().getType());
           lblManufaturer.setText(p.getManufacturer());
            }
        }
        
    }
    
    // Implement the actionlistener for the JButton.
    private class ButtonListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {
            
            // If a selection is made and a qualtity value is inputted.
          if(listParts.getSelectedValue() != null && !txtQuantity.getText().equals(""))
          {
              // If confirmed.
              if(JOptionPane.OK_OPTION == 
                    JOptionPane.showConfirmDialog(StockUpdatePanel.this, "Are you sure?",
                            "Make a choice", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE))
              {
              try
              {  
                  // Try to parse the quantity value to integer and add the quantity to the selected part.
                 Part p = (Part)listParts.getSelectedValue();
                 p.addQuantity(Integer.parseInt(txtQuantity.getText()));
    
              }
              catch(NumberFormatException ex)
              {
                  // Show an error if the value inserted is not a valid integer.
                  JOptionPane.showMessageDialog(StockUpdatePanel.this, "Invalid number.", "Error Message", JOptionPane.ERROR_MESSAGE);
              }
              // refresh the list parts.
              populatePartsList();
              }
          }
        }
        
    }
}
