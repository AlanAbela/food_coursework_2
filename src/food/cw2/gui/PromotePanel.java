/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import food.cw2.utility.Manager;
import food.cw2.utility.Mechanic;
import food.cw2.utility.Promotable;

/**
 *
 * @author Alan
 */
public class PromotePanel extends JPanel{
    
    private Main main;
    private JLabel lblTitle;
    private JList listMechanics;
    private DefaultListModel model;
    private JScrollPane scroll;
    private JLabel lblID;
    private JLabel lblName;
    private JLabel lblCurrentRank;
    private JButton btnPromote;
    
    
    public PromotePanel(Main main)
    {
        this.main = main;
        lblTitle = new JLabel("Mechanic Promote Screen");
        model = new DefaultListModel();
        listMechanics = new JList(model);
        scroll = new JScrollPane(listMechanics);
        lblID = new JLabel();
        lblName = new JLabel();
        lblCurrentRank = new JLabel();
        btnPromote = new JButton("Promote");
        
        setLayout();
        
        populateMechanicList();
        
        btnPromote.addActionListener(new ButtonListener());
        listMechanics.addListSelectionListener(new SelectionListener());
        
    }

    // Create layout.
    private void setLayout() {
        
        this.setLayout(new GridLayout(2,1));
        this.setBorder(OuterBorder.styledBorder());
        
        // Set upper part //
        JPanel upperPanel = new JPanel();
        upperPanel.setBorder(OuterBorder.styledBorder());
        upperPanel.setLayout(new BorderLayout());
        upperPanel.add(lblTitle, BorderLayout.NORTH);
        upperPanel.add(scroll, BorderLayout.CENTER);
        this.add(upperPanel);
        
        // Set lower part //
        JPanel lowerPanel = new JPanel();
        lowerPanel.setBorder(OuterBorder.styledBorder());
        lowerPanel.setLayout(new GridLayout(4,2));
        lowerPanel.add(new JLabel("ID No: "));
        lowerPanel.add(lblID);
        lowerPanel.add(new JLabel("Name: "));
        lowerPanel.add(lblName);
        lowerPanel.add(new JLabel("Current rank: "));
        lowerPanel.add(lblCurrentRank);
        lowerPanel.add(btnPromote);
        this.add(lowerPanel);      
    }
    
    // Clear labels' text.
    public void clear()
    {
        lblName.setText("");
        lblID.setText("");
        lblCurrentRank.setText("");
        listMechanics.clearSelection();
    }
    
    // Get mechanics that can be promoted.
    public void populateMechanicList()
    {
        model.clear();
        
        for(Promotable prom : main.getPromotableMechanics())
        {
            model.addElement(prom);
        }
    }
    
    // Implement the actionlistener for the Promote button.
    private class ButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
          if(listMechanics.getSelectedValue() != null && JOptionPane.OK_OPTION == 
                    JOptionPane.showConfirmDialog(PromotePanel.this, "Are you sure?",
                            "Make a choice", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE))
          {
            Promotable prom = (Promotable)listMechanics.getSelectedValue();
            Manager manager = main.getManager();
            manager.promoteMechanic(prom.ID());
            
            populateMechanicList();
          }
        }
        
    }
    
    // Implement the SelectionListener fot the JList.
    private class SelectionListener implements ListSelectionListener{

        @Override
        public void valueChanged(ListSelectionEvent e) {
            
           if(e.getValueIsAdjusting()){
               
               Mechanic mechanic = (Mechanic)listMechanics.getSelectedValue();
               lblName.setText(mechanic.getName());
               lblID.setText(mechanic.getID());
               lblCurrentRank.setText(mechanic.getSkill().toString());
               
               
           }
        }
        
    }
}
