/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import food.cw2.utility.Job;
import food.cw2.utility.JobStatus;
import food.cw2.utility.MechanicStatus;

/**
 *
 * @author Alan
 */
public class UpdateJobPanel extends JPanel{
    
    private JLabel lblTitle;
    private JList listJobs;
    private DefaultListModel model;
    private JLabel lblJobNo;
    private JLabel lblCustomerId;
    private JLabel lblDateCreated;
    private JLabel lblMechanicName;
    private JLabel lblJobType;
    private Main main;
    private JScrollPane scroll;
    private JButton btnComplete;
    


public UpdateJobPanel(Main main)
{
    
this.main = main;

lblTitle = new JLabel("Job Update Screen");
model = new DefaultListModel();
listJobs = new JList(model);
scroll = new JScrollPane(listJobs);
lblJobNo = new JLabel();
lblCustomerId = new JLabel();
lblDateCreated = new JLabel();
lblMechanicName = new JLabel();
lblJobType = new JLabel();
btnComplete = new JButton("Complete");

btnComplete.addActionListener(new ButtonListener());
listJobs.addListSelectionListener(new SelectionListener());

setLoyout();

populateOngoingJobList();

}

// Create layout.
private void setLoyout() {
        
        this.setLayout(new GridLayout(2,1));
        this.setBorder(OuterBorder.styledBorder());
        
        // Set upper part //
        JPanel upperPanel = new JPanel();
        upperPanel.setBorder(OuterBorder.styledBorder());
        upperPanel.setLayout(new BorderLayout());
        upperPanel.add(lblTitle, BorderLayout.NORTH);
        upperPanel.add(scroll, BorderLayout.CENTER);
        this.add(upperPanel);
        
        // Set lower part //
        JPanel lowerPanel = new JPanel();
        lowerPanel.setBorder(OuterBorder.styledBorder());
        lowerPanel.setLayout(new GridLayout(6,2));
        lowerPanel.add(new JLabel("Job No: "));
        lowerPanel.add(lblJobNo);
        lowerPanel.add(new JLabel("Customer ID: "));
        lowerPanel.add(lblCustomerId);
        lowerPanel.add(new JLabel("Date Issued: "));
        lowerPanel.add(lblDateCreated);
        lowerPanel.add(new JLabel("Mechanic Name: "));
        lowerPanel.add(lblMechanicName);
        lowerPanel.add(new JLabel("Job Type: "));
        lowerPanel.add(lblJobType);
        lowerPanel.add(btnComplete);
        this.add(lowerPanel);
    }

    // Updates the list of ongoing jobs.
    public void populateOngoingJobList()
    {
        model.clear();
        for(Job j : main.getOngoingJob())
        {
                model.addElement(j);           
        }
    }
    // Clears the labes.
    private void clearText()
    {
        lblJobNo.setText("");
        lblCustomerId.setText("");
        lblDateCreated.setText("");
        lblMechanicName.setText("");
        lblJobType.setText("");
    }
    
    // Implementation of the ListSelectionListener used for the JList.
    private class SelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e) {
            
            if(e.getValueIsAdjusting())
            {
                Job job = (Job)listJobs.getSelectedValue();
                
                lblJobNo.setText(String.valueOf(job.getNumber()));
                lblCustomerId.setText(job.getCustomer().getName());
                Date date = job.getDateCreated();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String dateToString = dateFormat.format(date);
                lblDateCreated.setText(dateToString);
                lblMechanicName.setText(job.getMechanic().getName());
                lblJobType.setText(job.getJobType().toString());
            }
        }     
    }
    
    // Implementation of the ActionLIstener used for the job complete button.
    private class ButtonListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {
            
            // If a selection is made and confirmed.
            if(listJobs.getSelectedValue()!=null && JOptionPane.OK_OPTION == 
                    JOptionPane.showConfirmDialog(UpdateJobPanel.this, "Are you sure?",
                            "Make a choice", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE))
            {
                Job job = (Job)listJobs.getSelectedValue(); // get selected job.
                job.setStatus(JobStatus.COMPLETE); // Set job as complete.
                job.getMechanic().getID(); // Get the id of the machanic which is on the job.
                main.getMechanicByID(job.getMechanic().getID()).setStatus(MechanicStatus.IDLE); // Set status of the mechanic to idle.
                job.setDateCompleted(new Date()); // Set completion date.
                
                // clear Text.
                clearText();
                
                // refresh pending job list.
                populateOngoingJobList();
            }
        }
        
    }
    
}