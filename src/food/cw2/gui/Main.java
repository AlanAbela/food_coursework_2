/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui;

import food.cw2.gui.jobregister.RegisterFormListener;
import food.cw2.gui.jobregister.JobRegisterFormEvent;
import food.cw2.gui.jobregister.JobRegisterPanel;
import food.cw2.gui.findjob.FindJobPanel;
import food.cw2.gui.findjob.FindJobListener;
import food.cw2.logic.Controller;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import food.cw2.utility.Job;
import food.cw2.utility.Manager;
import food.cw2.utility.Mechanic;
import food.cw2.utility.Part;
import food.cw2.utility.Promotable;
import javax.swing.JLabel;

/**
 *
 * @author Alan
 */
public class Main extends JFrame{
    
       private Controller controller = new Controller();
    
    // Create the central panels.
       private JobRegisterPanel jobRegistrationPanel = new JobRegisterPanel(controller.getVehicleTypes(), controller.getPartTypes(), this);
       private FindJobPanel findJobPanel = new FindJobPanel(this);
       private CompleteJobPanel completeJobPanel = new CompleteJobPanel(this);
       private StockUpdatePanel stockUpdatePanel = new StockUpdatePanel(this);
       private UpdateJobPanel updateJobpanel = new UpdateJobPanel(this);
       private MonthlyReportPanel monthlyReportPanel = new MonthlyReportPanel(this);
       private PromotePanel promotePanel = new PromotePanel(this);
    
    // Create the side menu buttons.
       private JButton btnRegisterJob = new JButton("Register Job");
       private JButton btnSearchJob = new JButton("Job Detail");
       private JButton btnFindJob = new JButton("Find Job");
       private JButton btnUpdateJob = new JButton("Update Job");
       private JButton btnStockUpdate = new JButton("View/Update Stock");
       private JButton btnGenerateReport = new JButton("Generate Report");
       private JButton btnPromote = new JButton("Promote");
       
       private JPanel cardsPanel = new JPanel();
        

    public Main()
    {
      super("Garage management system");     
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      // Add action listeners.
      ButtonListener buttonListener = new ButtonListener();
      btnRegisterJob.addActionListener(buttonListener);
      btnSearchJob.addActionListener(buttonListener);
      btnFindJob.addActionListener(buttonListener);
      btnUpdateJob.addActionListener(buttonListener);
      btnStockUpdate.addActionListener(buttonListener );
      btnGenerateReport.addActionListener(buttonListener);
      btnPromote.addActionListener(buttonListener);
         
      // Create layout.
      this. setLayout(new BorderLayout());
      this.add(sideMenu(), BorderLayout.WEST);
      
      cardsPanel.setLayout(new CardLayout());
      cardsPanel.add(jobRegistrationPanel, "0");
      cardsPanel.add(completeJobPanel, "6");
      cardsPanel.add(findJobPanel, "1");
      cardsPanel.add(stockUpdatePanel, "2");
      cardsPanel.add(updateJobpanel, "3");
      cardsPanel.add(monthlyReportPanel, "4");
      cardsPanel.add(promotePanel, "5");
      this.add(cardsPanel, BorderLayout.CENTER);
      
      this.setVisible(true);
       this.pack();
       
       jobRegistrationPanel.setFormListener(new RegisterFormListener(){
          @Override
          public void formEventOccured(JobRegisterFormEvent ev) {
              String name = ev.getName();
              String ID = ev.getID();
              String vehicleType = ev.getVehicleType();
              String partType = ev.getPartType();
              
              controller.addJob(name, ID, vehicleType, partType);
          }
      });
       
       findJobPanel.setListener(new FindJobListener(){

          @Override
          public List<Job> jobsEmitted() {
          return controller.getPendingJobs();
          }
      });
    }
    
    private Component sideMenu()
    {
        // Create the Container to hold the side buttons
        JPanel sidePanel = new JPanel();
        sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));
        
        // Set a border.
        sidePanel.setBorder(OuterBorder.styledBorder());       
        
        // Make the buttons equal in size.
        btnRegisterJob.setMaximumSize(new Dimension(1000, btnRegisterJob.getMinimumSize().height));
        btnSearchJob.setMaximumSize(new Dimension(1000, btnRegisterJob.getMinimumSize().height));
        btnFindJob.setMaximumSize(new Dimension(1000, btnRegisterJob.getMinimumSize().height));
        btnUpdateJob.setMaximumSize(new Dimension(1000, btnRegisterJob.getMinimumSize().height));
        btnStockUpdate.setMaximumSize(new Dimension(1000, btnRegisterJob.getMinimumSize().height));
        btnGenerateReport.setMaximumSize(new Dimension(1000, btnRegisterJob.getMinimumSize().height));
        btnPromote.setMaximumSize(new Dimension(1000, btnRegisterJob.getMinimumSize().height));
        
        // Add the buttons into the layout plus some space in between.
        sidePanel.add(new JLabel("Customer Area"));
        sidePanel.add(btnRegisterJob);
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(btnSearchJob);
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(new JLabel("Mechanic Area"));     
        sidePanel.add(btnFindJob);
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(btnUpdateJob);
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(new JLabel("Stock Manager Area"));  
        sidePanel.add(btnStockUpdate);
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(new JLabel("Manager Area"));  
        sidePanel.add(btnGenerateReport);
        sidePanel.add(Box.createRigidArea(new Dimension(5,5)));
        sidePanel.add(btnPromote);
     
        return sidePanel;
    }
        
    public BigDecimal getCost(String vehicleType, String partType)
    {
        return controller.getPartCost(vehicleType, partType);
    }
    
    private List getJobList()
    {
        return controller.getPendingJobs();
    }

    // <editor-fold defaultstate="collapsed" desc="EventListeners">

    private class ButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {         
            CardLayout cl = (CardLayout)cardsPanel.getLayout();
            
               if(e.getSource().equals(btnRegisterJob))
             {
             cl.show(cardsPanel, "0");
             }
            
              if(e.getSource().equals(btnSearchJob))
             {
                 completeJobPanel.clear();
             cl.show(cardsPanel, "6");
             }
               
             if(e.getSource().equals(btnFindJob))
             {
               findJobPanel.refresh();
              cl.show(cardsPanel, "1");        
             }
          
             if(e.getSource().equals(btnStockUpdate))
             {
                 cl.show(cardsPanel, "2");
             }
             
             if(e.getSource().equals(btnUpdateJob))
             {
                 updateJobpanel.populateOngoingJobList();
                 cl.show(cardsPanel, "3");
             }
             
             if(e.getSource().equals(btnGenerateReport))
             {
                 cl.show(cardsPanel, "4");
                 monthlyReportPanel.clearText();
             }
             
             if(e.getSource().equals(btnPromote))
             {
                 cl.show(cardsPanel, "5");
                 promotePanel.clear();
             }
        }      
    }
    
           public List getAvailableMechanics()
        {
            return controller.getMechanics();
        }
    
           public List getMechanicsByFiler(String jobType)
           {
              return controller.filterMechanics(jobType);
           }
           
           public boolean isPartAvailable(String partType, String vehicleType)
           {
               return controller.isPartAvailabe(partType, vehicleType);
           }
           
           public void takePart(String partType, String vehicleType)
           {
               controller.takePart(partType, vehicleType);
           }
           
           public void setJobOngoing(int jobNo)
           {
               controller.setJobOngoing(jobNo);
           }
           
           public Set<Part> getParts()
           {
             return  controller.getParts();
           }
           
           public Job getJob(int jobNo)
           {
               return controller.getJob(jobNo);
           }
           
           public List<Job> getOngoingJob()
           {
               return controller.getOngoingJobs();
           }
           
           public Manager getManager()
           {
               return controller.getManager();
           }
           
           public List<Promotable> getPromotableMechanics()
           {
               return controller.getPromotableMechanics();
           }
           
           public Mechanic getMechanicByID(String id)
           {
               return controller.getMechanicByID(id);
           }
           
           public Job getCompleteJob(int jobNo)
           {
               return controller.getCompleteJob(jobNo);
           }
// </editor-fold>
}
