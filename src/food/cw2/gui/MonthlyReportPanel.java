/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import food.cw2.utility.Manager;
import food.cw2.utility.Report;

/**
 *
 * @author Alan
 */
public class MonthlyReportPanel extends JPanel{
    
    private JLabel lblTitle;
    private JLabel lblMonth;
    private JTextField txtMonth;
    private JTextArea txtArea;
    private JScrollPane scroll;
    private JButton btnReport;
    private Main main;
    
    public MonthlyReportPanel (Main main)
    {
        this.main = main;
        lblTitle = new JLabel("Report Generator Panel");
        lblMonth = new JLabel("Input Month No: ");
        txtMonth = new JTextField(4);
        txtArea = new JTextArea();
        txtArea.setEnabled(false);
        scroll = new JScrollPane(txtArea);
        btnReport = new JButton("Generate Report");
        
        btnReport.addActionListener(new ButtonListener());
        
        setLayout();
    }

    // Create Layout.
    private void setLayout() {
        
        this.setLayout(new BorderLayout());
        this.setBorder(OuterBorder.styledBorder());
        
        // Set upper part //
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new GridLayout(2,1));
        topPanel.add(lblTitle);
        JPanel monthPanel = new JPanel();
        monthPanel.setLayout(new FlowLayout());     
        monthPanel.add(lblMonth);
        monthPanel.add(txtMonth);
        topPanel.add(monthPanel);
        this.add(topPanel, BorderLayout.NORTH);
        
        // Set lower part //
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BorderLayout());
        textPanel.add(txtArea, BorderLayout.CENTER);
        textPanel.setBorder(OuterBorder.styledBorder());
        this.add(textPanel, BorderLayout.CENTER);
        this.add(btnReport, BorderLayout.SOUTH);
          
    }
    
    // Clear Text.
    public void clearText()
    {
        txtArea.setText("");
        txtMonth.setText("");
    }
    
    // Implement the Action listenr for the generate report button.
    private class ButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            // clear text.
            txtArea.setText("");
            
            try
            {
                // Try parsing the value inputed in the month field.
                int month = Integer.parseInt(txtMonth.getText());
                // Validate that the value is not larger than 12.
                if(month > 12) throw new NumberFormatException();
                // Get a reference to the Manager class.
                Manager manager = main.getManager();
                // Generate report by calling the generateMonthlyReport on the manager reference.
                Report report = manager.generateMonthlyReport(month-1);
                
                // Show the report on the Panel, by passing the report values to the textArea.
                 txtArea.setText("Monthly Report for month: " + month +" \n\n No Of Tyre repair jobs: " + report.getNoOfTyreJob()+
                         "\n No of Exhaust repair jobs: " + report.getNoOfExaustJob() +
                         "\n No of Engine repair jobs: " + report.getNoOfNewEngineJob());                               
            }
            catch(NumberFormatException ex)
                    {
                        JOptionPane.showMessageDialog(MonthlyReportPanel.this, "Invalid number.", "Error Message", JOptionPane.ERROR_MESSAGE);
                    }
            
            txtMonth.setText("");
        }
        
    }
}
