/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

/**
 *
 * @author Alan
 */
public class OuterBorder {
    
     public static Border styledBorder()
    {
      // Create a nice border for the Component (a good example of the Factory Pattern here).
        Border emptyBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        Border etchedBorder = BorderFactory.createEtchedBorder();
        Border border= BorderFactory.createCompoundBorder(emptyBorder, etchedBorder);
        
        return border;
    }
    
}
