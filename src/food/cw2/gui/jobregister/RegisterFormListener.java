/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui.jobregister;

import food.cw2.gui.jobregister.JobRegisterFormEvent;

/**
 *
 * @author Alan
 */
public interface RegisterFormListener {
    
    public void formEventOccured(JobRegisterFormEvent ev);
    
}
