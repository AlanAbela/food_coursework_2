/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui.jobregister;

import food.cw2.gui.Main;
import food.cw2.gui.OuterBorder;
import food.cw2.gui.findjob.JobDetailsPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.math.BigDecimal;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Alan
 */
public class JobRegisterPanel extends JPanel{
    
     // Create JobRegister components.
       private JLabel lblCustomerName = new JLabel("Name: ");
       private JLabel lblCustomerID = new JLabel("ID card No: ");
       private JLabel lblVehicleType = new JLabel("Vehicle: ");
       private JLabel lblPartType = new JLabel("Part: ");
       private JLabel lblCost = new JLabel("Cost: ");
       private JTextField txtCustomerName = new JTextField(10);
       private JTextField txtCustomerID = new JTextField(10);
       private JComboBox ddlVehicleType;
       private JComboBox ddlPartType;
       private JTextField txtCost = new JTextField(10);
       private JButton btnRegister = new JButton("Confirm");
       private JButton btnClear = new JButton("Clear");
       private JLabel lblTitle = new JLabel("Job Details");
       private String vehicleType;
       private String partType;
       
       private RegisterFormListener registerFormListener;
       private Main main;
    
       
       public JobRegisterPanel (List ddlVehicleTypeSource, List ddlPartTypeSource, Main main)
       {
           this.main = main;
         ddlVehicleType = new JComboBox();
         ddlPartType = new JComboBox();        
         
         // Populate the Vehicle JComboBox..
         for(Object o : ddlVehicleTypeSource)
         {
           ddlVehicleType.addItem(o.toString());
         }
         
         // Populate the Parts JComboBox.
         for(Object o : ddlPartTypeSource)
         {
           ddlPartType.addItem(o.toString());
         }
          
         // Get the cost of the part.
         BigDecimal cost = main.getCost(ddlVehicleType.getSelectedItem().toString(), ddlPartType.getSelectedItem().toString());
         txtCost.setText(cost.toString());
           
         // Layout the components.
         createLayout();
           
         // Pass action listener to the registration button.
        btnRegister.addActionListener(new ActionListener()
        {
             @Override
             public void actionPerformed(ActionEvent e) 
             {
                 if(JOptionPane.showConfirmDialog(JobRegisterPanel.this, "Are you sure?", "Job confirmation",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION)
                 {
                 String name = txtCustomerName.getText();
                 String ID = txtCustomerID.getText();
                 String vehicleType = (String)ddlVehicleType.getSelectedItem();
                 String partType = (String)ddlPartType.getSelectedItem();
                 // The JobRegisterFormEvent is used to not pass the values directly to the main class.
                 //This was thought to reduce coupling wit hte Main class.
                 JobRegisterFormEvent event = new JobRegisterFormEvent(name, ID, vehicleType, partType);
                 
                 if(registerFormListener != null)
                 {
                     registerFormListener.formEventOccured(event); // Calling this method passes the values to the main method,
                                                                   // which in turn passes the values to the logic.
                 }
                }
             }
         });
            
        // Add item listener to the Vehicle Type JComboBox.
        ddlVehicleType.addItemListener(new ItemListener(){

             @Override
             public void itemStateChanged(ItemEvent e) {
                 vehicleType = e.getItem().toString();
                 partType = ddlPartType.getSelectedItem().toString();
                 BigDecimal decimal = main.getCost(vehicleType, partType);
                 txtCost.setText(decimal.toString());
             }
         });
        
        // Add item listener to the Part Type JComboBox.
        ddlPartType.addItemListener(new ItemListener(){

             @Override
             public void itemStateChanged(ItemEvent e) {
                 partType = e.getItem().toString();
                 vehicleType = ddlVehicleType.getSelectedItem().toString();
                 BigDecimal decimal = main.getCost(vehicleType, partType);
                 txtCost.setText(decimal.toString());
             }
         });
        
       }

public void setFormListener(RegisterFormListener listener){
          this.registerFormListener = listener;
    }

public String getVehicleType() {
        return vehicleType;
    }

public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

public String getPartType() {
        return partType;
    }

public void setPartType(String partType) {
        this.partType = partType;
    }

    private void createLayout() {
         setLayout(new BorderLayout());
        setBorder(OuterBorder.styledBorder());
        
        JPanel centerArea = new JPanel();
        JPanel titleArea = new JPanel();
        JPanel buttonArea = new JPanel();
        
        centerArea.setLayout(new GridLayout(5,2));
        titleArea.setLayout(new GridLayout(1,1));
        buttonArea.setLayout(new FlowLayout(FlowLayout.CENTER));
        
        add(centerArea, BorderLayout.CENTER);
        add(titleArea, BorderLayout.NORTH);
        add(buttonArea, BorderLayout.SOUTH);
        
        titleArea.add(lblTitle);
        
        centerArea.add(lblCustomerID);
        centerArea.add(txtCustomerID);
        centerArea.add(lblCustomerName);
        centerArea.add(txtCustomerName);
        centerArea.add(lblVehicleType);
        centerArea.add(ddlVehicleType);
        centerArea.add(lblPartType);
        centerArea.add(ddlPartType);
        centerArea.add(lblCost);
        centerArea.add(txtCost);
        
        buttonArea.add(btnClear);
        buttonArea.add(btnRegister);
    }
       
}
