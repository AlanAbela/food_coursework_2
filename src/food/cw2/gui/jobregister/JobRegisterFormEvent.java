/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui.jobregister;

/**
 *
 * @author Alan
 */
public class JobRegisterFormEvent {
    
    private String name;
    private String ID;
    private String cost;
    private String vehicleType;
    private String partType;
    
    public JobRegisterFormEvent(String name, String ID, String vehicleType, String partType)
    {
        this.name = name;
        this.ID = ID;
        this.vehicleType = vehicleType;
        this.partType = partType;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getVehicleType() {
        return vehicleType;
    }


    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }
    
}
