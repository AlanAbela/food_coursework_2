/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui.findjob;

import food.cw2.gui.Main;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import food.cw2.utility.Mechanic;
import food.cw2.utility.MechanicStatus;


/**
 *
 * @author Alan
 */
public class JobDetailsPanel extends JFrame{
    
    private JLabel lblCustomerName;
    private JLabel lblCustomerID;
    private JLabel lblType;
    private JLabel lblDate;
    private JComboBox ddlMechanics;
    private JButton button;
    private Main main;
    private String partType;
    private String vehicleType;
    private int jobNo;
    private JobListListener changeListener;
    
    
    public JobDetailsPanel(String name, String ID, String jobType, int jobNo, Date date, List ddlMechanicSource, String partType, String vehicleType, Main main, JobListListener changeListener){
        
        super("Job Details Panel");
        
        this.main = main;
        this.partType = partType;
        this.vehicleType = vehicleType;
        this.jobNo = jobNo;
        this.changeListener = changeListener;
        
        lblCustomerName = new JLabel(name);
        lblCustomerID = new JLabel(ID);
        lblType = new JLabel(jobType);
        ddlMechanics  = new JComboBox(main.getMechanicsByFiler(jobType).toArray());
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateToString = dateFormat.format(date);
        lblDate = new JLabel(dateToString);
        
        button = new JButton("Select");
        button.addActionListener(new ButtonListener());
        
        setLayout();
              
    }
    
    private void setLayout()
    {
        this.setLayout(new BorderLayout());
        
        // Set a label to the panel.
        this.add(new JLabel("Job Details"), BorderLayout.NORTH);
        
        // Set information panel
        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new GridLayout(5,2));
        infoPanel.add(new JLabel("Customer name: "));
        infoPanel.add(getLblCustomerName());
        infoPanel.add(new JLabel("Customer ID: "));
        infoPanel.add(getLblCustomerID());
        infoPanel.add(new JLabel("Date issued: "));
        infoPanel.add(getLblDate());
        infoPanel.add(new JLabel("Select Mechanic: "));
        infoPanel.add(getDdlMechanics());
        this.add(infoPanel, BorderLayout.CENTER);
        
        // Setting Button
        this.add(getButton(), BorderLayout.SOUTH);
        
        this.pack();
        
    }

    public JLabel getLblCustomerName() {
        return lblCustomerName;
    }

    public JLabel getLblCustomerID() {
        return lblCustomerID;
    }

    public JLabel getLblType() {
        return lblType;
    }

    public JLabel getLblDate() {
        return lblDate;
    }

    public JComboBox getDdlMechanics() {
        return ddlMechanics;
    }

    public JButton getButton() {
        return button;
    }

    /**
     * @param partType the partType to set
     */
    public void setPartType(String partType) {
        this.partType = partType;
    }

    /**
     * @param vehicleType the vehicleType to set
     */
    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public void setJobNo(int jobNo) {
        this.jobNo = jobNo;
    }

    // Implementation of the Actionlistener.
    private class ButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
              
            // If a mechanic is selected from the JComboBox.
            if(ddlMechanics.getItemCount() != 0)
            {
            // If the selection is confirmed.
            if(JOptionPane.showConfirmDialog(JobDetailsPanel.this, "Do you want to take up this Job?", "Job confirmation",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION)
            {
                // Pass the values and call the update method that is implemented by the FindJobPanel instance.
            changeListener.update(jobNo, ddlMechanics.getSelectedItem(), vehicleType, partType);
            setVisible(false);
            }           
            }
            // If no mechanics are available Show an error message.
            else
            {
                JOptionPane.showMessageDialog(JobDetailsPanel.this, "No mechanics Available", "Error message", JOptionPane.ERROR_MESSAGE);
            }
        }
        
    }
}
