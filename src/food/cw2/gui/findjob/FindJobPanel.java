/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui.findjob;

import food.cw2.gui.Main;
import food.cw2.gui.OuterBorder;
import food.cw2.logic.Controller;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import food.cw2.utility.Job;
import food.cw2.utility.Mechanic;
import food.cw2.utility.MechanicStatus;

/**
 *
 * @author Alan
 */
public class FindJobPanel extends JPanel implements JobListListener{

     // Create Pending Jobs Panel components.
       private JLabel lblPendingJobs = new JLabel("Pending Jobs.");
       private JList  pendingJobs;
       DefaultListModel model;
       private JScrollPane scrollPane;
       private JButton Select;
       
       private JobDetailsPanel detailsPanel;
       
       private FindJobListener findJobListener;
       
       private Main main;
    
       
       
  public FindJobPanel(Main main){
      
      this.main = main;
      
      model = new DefaultListModel();
      pendingJobs = new JList(model);
      scrollPane = new JScrollPane(pendingJobs);
      Select = new JButton("Select");
      
      createLayout();
     
     Select.addActionListener(new ButtonListener());
  }
  
  // Layout the panel //
  private void createLayout()
  {
       setLayout(new BorderLayout());
     setBorder(OuterBorder.styledBorder());
     add(lblPendingJobs, BorderLayout.NORTH);
     add(scrollPane, BorderLayout.CENTER);
     add(Select, BorderLayout.SOUTH);
  }
  
  // Setter for the FindJobListener variable.
  public void setListener(FindJobListener listener)
  {
      this.findJobListener = listener;
  }
  
  // Update the JList //
  public void refresh()
  {
      if(findJobListener != null)
     {
         model.clear();
         for(int i = 0; i<findJobListener.jobsEmitted().size(); i++)
         {
            model.add(i, findJobListener.jobsEmitted().get(i));
         }      
     } 
  }

  /**
   * Updates the list of pending jobs when a job is accepted from the Job Details panel.
   * @param jobNo 
   */
    @Override
    public void update(int jobNo, Object mechanic, String partType, String vehicleType) {
        main.setJobOngoing(jobNo);      
        main.takePart(partType, vehicleType);
        Mechanic m = (Mechanic)mechanic;
         main.getJob(jobNo).setMechanic(m);
        m.setStatus(MechanicStatus.BUSY);
        refresh();
    }
  
    // Action listener implementation.
   private class ButtonListener implements ActionListener
        {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            // If an instance of the details panel is already in memory hide it.
            if(detailsPanel != null)
            {
                detailsPanel.setVisible(false);              
            }
            
            // If a selection from the Job list is made.
            if(pendingJobs.getSelectedValue() != null)
            {
                Job job = (Job)pendingJobs.getSelectedValue();
                int jobNo = job.getNumber();
                String name = job.getCustomer().getName();
                String ID = job.getCustomer().getID();
                String jobType = job.getJobType().toString();
                Date date = job.getDateCreated();
                List<Mechanic> mechanics = main.getMechanicsByFiler(jobType);
                String partType = job.getCustomer().getRepair().getPartType().toString();
                String vehicleType = job.getCustomer().getRepair().getVehicleType().toString();
                boolean isPartAvailable = main.isPartAvailable(partType, vehicleType);
                
                // If there is no instance of the details panel, create a new one.
                if(detailsPanel == null)
                {
                    detailsPanel = new JobDetailsPanel(name, ID, jobType, jobNo, date, mechanics, partType, vehicleType, main, FindJobPanel.this);
                    if(!isPartAvailable) // If part is not available, disable the detailsPanel Slect button.
                    {
                        detailsPanel.getButton().setText("Parts Not Available");
                        detailsPanel.getButton().setEnabled(false);
                    }
                    else // If there is one or more parts eneable the button and set text accordingly.
                    {
                        detailsPanel.getButton().setText("Select");
                        detailsPanel.getButton().setEnabled(true);
                    }
                    detailsPanel.setVisible(true);
                }
                
                // If a details panel instance is already in memory change its values.
                if(detailsPanel != null)
                {
                    detailsPanel.getLblCustomerName().setText(name);
                    detailsPanel.getLblCustomerID().setText(ID);
                    detailsPanel.getLblType().setText(jobType);
                    detailsPanel.setPartType(partType);
                    detailsPanel.setVehicleType(vehicleType);
                    detailsPanel.setJobNo(jobNo);
                    
                    // Update mechanic list.
                    detailsPanel.getDdlMechanics().removeAllItems();
                    for(Mechanic m : mechanics)
                    {
                        detailsPanel.getDdlMechanics().addItem(m);                       
                    }
                    
                    // Check if part is available.
                    if(!isPartAvailable)
                    {
                        detailsPanel.getButton().setText("Parts Not Available");
                        detailsPanel.getButton().setEnabled(false);
                        System.out.println("hi");
                    }
                    else
                    {
                        detailsPanel.getButton().setText("Select");
                        detailsPanel.getButton().setEnabled(true);
                    }
                    
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    String dateToString = dateFormat.format(date);
                    detailsPanel.getLblDate().setText(dateToString);
                    
                    detailsPanel.setVisible(true);
                }  
            }
        }
       
   }
  
}
