/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.gui.findjob;

/**
 *
 * @author Alan
 */
public interface JobListListener {
    
    public void update(int jobNo, Object mechanic, String vehicleType, String partType);
    
    
}
