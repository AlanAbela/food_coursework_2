/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.logic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import food.cw2.utility.Data;
import food.cw2.utility.Ford;
import food.cw2.utility.Job;
import food.cw2.utility.JobStatus;
import food.cw2.utility.Manager;
import food.cw2.utility.Mechanic;
import food.cw2.utility.MechanicStatus;
import food.cw2.utility.PartType;
import food.cw2.utility.PartTypeFactory;
import food.cw2.utility.Promotable;
import food.cw2.utility.Rover;
import food.cw2.utility.Skill;
import food.cw2.utility.VehicleType;
import food.cw2.utility.VehicleTypeFactory;

/**
 *
 * @author Alan
 */
public class Controller {
    
    // Create an instance of the Data class.
    Data data = new Data();
    
    // Method to add Job.
    public void addJob(String name, String ID, String vehicleType, String partType)
    {
        data.addJob(name, ID, VehicleTypeFactory.getVehicleType(vehicleType), PartTypeFactory.getPartType(partType));
    }
    
    /**
     * Retrieves a list of PartTypes.
     * @return 
     */
    public List<PartType> getPartTypes()
    {
      return  data.getPartTypes();
    }
    
    /**
     * Retrieves a list of vehicle types.
     * @return 
     */
    public List<VehicleType> getVehicleTypes()
    {
       List<VehicleType> list = new ArrayList<VehicleType>();
       list.add(VehicleTypeFactory.getVehicleType("ROVER"));
       list.add(VehicleTypeFactory.getVehicleType("FORD"));
       
       return list;
    }
    
    /**
     * Retrieve cost of a part.
     * @param vehicle
     * @param part
     * @return 
     */
    public BigDecimal getPartCost(String vehicle, String part)
    {
        return data.getCost(vehicle, part);
    }
    
    /**
     * Retrieves a list of pending jobs.
     * @return List object.
     */
    public List<Job> getPendingJobs()
    {
        List<Job> pendingJobs = new ArrayList();
        List<Job> allJobs = data.getJobs();
        
        for(Job job : allJobs)
        {
            if(job.getStatus() == JobStatus.PENDING)
            {
                pendingJobs.add(job);
            }
        }
        
        return pendingJobs;
    }
    
    /**
     * Retrieves a list of available mechanics.
     * @return 
     */
    public List getMechanics()
    {
        return data.getAvailableMechanics();
    }
    
     /**
     * Gets the mechanics that are able to do the job.
     * @param jobType
     * @param list
     * @return 
     */
    public List filterMechanics(String jobType)
    {
        List<Mechanic> output = new ArrayList();
        
        if(jobType.equals("ENGINE"))
        {
            for(Mechanic m : data.getAvailableMechanics())
            {
                if(m.getSkill() == Skill.MASTER)
                {
                    output.add(m);
                }
            }
        } 
        else if(jobType.equals("EXHAUST"))
        {
            for(Mechanic m : data.getAvailableMechanics())
            {
                if((m.getSkill() == Skill.MASTER || m.getSkill() == Skill.WORKMAN))
                {
                    output.add(m);
                }
            }
        }
        else
        {
            output = data.getAvailableMechanics();
        }
        
        return output;
    }
    
    /**
     * Check if part is available.
     * @param partType
     * @param vehicleType
     * @return 
     */
    public boolean isPartAvailabe(String partType, String vehicleType)
    {
        if(data.getStockManager().getNoOfPartsByType(partType, vehicleType) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Remove a part from the list.
     * @param partType
     * @param vehicleType 
     */
    public void takePart(String partType, String vehicleType)
    {
        data.takePart(partType, vehicleType);
    }
    
    public void setJobOngoing(int jobNo)
    {
        data.setJobOngoing(jobNo);
    }
    
    public void setJobComplete(int jobNo)
    {
        data.setComplete(jobNo);
    }

    public Set getParts() {
       
      return  data.getStockManager().getParts();
    }
    
    public Job getJob(int jobNo)
    {
        return data.getJob(jobNo);
    }
    
    public List<Job> getOngoingJobs()
    {
        return data.getOngoingJobs();
    }
    
    public Manager getManager()
    {
        return data.getManager();
    }
    
    public List<Promotable> getPromotableMechanics()
    {
        return data.getPromotable();
    }
    
    public Mechanic getMechanicByID(String ID)
    {
        return data.getMechanicByID(ID);
    }
    
    public Job getCompleteJob(int jobNo)
    {
        return data.getCompletedJob(jobNo);
    }
}
