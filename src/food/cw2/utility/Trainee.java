/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public class Trainee extends Mechanic implements Promotable{

    public Trainee(String ID, String name) {
        super(ID, name, Skill.TRAINEE);
    }

    /**
     * Promotes to next level.
     * @return Workman object representation of this instance.
     */
    @Override
    public Workman getPromotion() {
        
       Workman workman  = new Workman(getID(), getName());
       workman.setStatus(getStatus());
       workman.setJob(getJob());
       
       return workman;
   
    }

    @Override
    public String ID() {
        return getID();
    }
    
}
