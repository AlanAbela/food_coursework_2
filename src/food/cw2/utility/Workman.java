/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public class Workman extends Mechanic implements Promotable{

    public Workman(String ID, String name) {
        super(ID, name, Skill.WORKMAN);
    }

    /**
     * Promotes to next level.
     * @return a Master object representation of this instance.
     */
    @Override
    public Master getPromotion() {
         
        Master master = new Master(getID(), getName());
        master.setStatus(getStatus());
        master.setJob(getJob());
        
        return master;
        
    }

    @Override
    public String ID() {
        return getID();
    }
    
    
    
}
