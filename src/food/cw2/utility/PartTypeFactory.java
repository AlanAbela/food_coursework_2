/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public abstract class PartTypeFactory 
{

    /**
     * Produces an instance of a PartTpye.
     * @param type determines the instance type returned.
     * @return PartType object.
     */
    public static PartType getPartType(String type)
    {
        if(type.equalsIgnoreCase("TYRE"))
        {
            return new Tyre();
        }
        
        if(type.equalsIgnoreCase("EXHAUST"))
        {
            return new Exhaust();
        }
        
        if(type.equalsIgnoreCase("ENGINE"))
        {
            return new Engine();
        }
        
        return null;
    }
    
}
