/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Alan
 */
public class StockManager {
    
    private Set<Part> parts;
    private static StockManager stockManager;
    
    private StockManager(){
    parts = new TreeSet();
    };
    
    public static StockManager getInstance()
    {
        if(stockManager == null)
        {
            StockManager st = new StockManager();
            stockManager = st;
            return stockManager;
        }
        else
        {
            return stockManager;
        }      
    }

    public int getNoOfPartsByType(String partType, String vehicleType)
    {
        int count = 0;
        
        for(Part p : parts)
        {
            if(p.getPartType().getType().toString().equals(partType) && p.getVehicleType().getType().toString().equals(vehicleType))
            {
                count = p.getQuantity();
            }
        }        
        return count;
    }
    
    public Set<Part> getParts()
    {
        return parts;
    }
   
    /**
     * Removes a single quantity item from a defined part.
     * @param partNo part number.
     */
    public void takeItem(String partType, String vehicleType)
    {
        Iterator<Part> iterator = parts.iterator();
        
        while(iterator.hasNext())
        {
            Part p = iterator.next();
            
            if(p.getPartType().toString().equals(partType) && p.getVehicleType().toString().equals(vehicleType) && p.getQuantity() >= 1)
            {
                p.decrementQuantity();
             
            }
        }
        
    }
    
    /**
     * Retrieves parts that meet the defined quantity.
     * @param quantity
     * @return 
     */
    public Set<Part> getPartsByQuantity(int quantity)
    {
        Set<Part> partFiltered = new TreeSet<Part>();
        
        Iterator<Part> iterator = parts.iterator();
        
        while(iterator.hasNext())
        {
          Part p = iterator.next();
          
          if(p.getQuantity() == quantity)
          {
              partFiltered.add(p);
          }
        }
        
        return partFiltered;
    }
    
    public void addPart(Part part)
    {
        parts.add(part);
    }
}
