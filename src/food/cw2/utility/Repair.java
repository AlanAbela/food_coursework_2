/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public class Repair {
    
    private VehicleType vehicleType;
    private PartType partType;
    
    public Repair(VehicleType vehicleType, PartType partType)
    {
        this.vehicleType = vehicleType;
        this.partType = partType;
    }
    
    public VehicleType getVehicleType()
    {
        return vehicleType;
    }
    
    public PartType getPartType()
    {
        return partType;
    }
    
}
