/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Alan
 */
public class Manager {
    
    private List<Job> jobs;
    private List<Mechanic> mechanics;
    
    public void setJobs(List jobs)
    {
        this.jobs = jobs;
    }
    
    // Generate current year's selected month report.
    public Report generateMonthlyReport(int month)
    {    
        Report report = new Report();
        
        Calendar cal = Calendar.getInstance();
        // Get the current year.
        int year = cal.get(Calendar.YEAR);
        
        List<Job> completedJobs = new ArrayList();
        
        for(Job j : jobs)
        {
            if(j.getStatus() == JobStatus.COMPLETE)
            {
                completedJobs.add(j);
            }
        }
        
        for(Job j : completedJobs)
        {
               
            cal.setTime(j.getDateCompleted());

            // If job's month and year match.
            if(month == cal.get(Calendar.MONTH) && year == cal.get(Calendar.YEAR))
            {
                // Increment report values according to job type.
             if(j.getJobType() == JobType.TYRE)
             {
                 report.incrementNoOfTyreJob();
             }
             
             if(j.getJobType() == JobType.EXHAUST)
             {
                 report.incrementNoOfExaustJob();
             }
             
             if(j.getJobType() == JobType.ENGINE)
             {
                 report.incrementNoOfNewEngineJob();
             }
             
            }
          
          }
            
        
     return report;
    }
    
    public void promoteMechanic(String promotableID)
    {
        for(int i = 0; i<mechanics.size(); i++)
        {
            if(mechanics.get(i).getID().equalsIgnoreCase(promotableID))
            {
                if(mechanics.get(i) instanceof Promotable)
                {
                    Promotable p = (Promotable)mechanics.get(i);
                    
                    // Replace with a promoted version.
                    mechanics.set(i, p.getPromotion());
                                      
                }
            }
        }
    }
    
    public void setMechanics(List mechanics)
    {
        this.mechanics = mechanics;
    }
    
}
