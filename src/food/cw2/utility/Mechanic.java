/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public abstract class Mechanic {
    
    private String ID;
    private String name;
    private Skill skill;
    private Job job;
    private MechanicStatus status;
    
    public Mechanic(String ID, String name, Skill skill){
    this.name = name;
    this.ID = ID;
    this.skill = skill;
    status = MechanicStatus.IDLE;
    
    }
    
public  void setName(String name)
{
    this.name = name;
}

public  String getName()
{
 return name;   
}

public  void setID(String ID)
{
    this.ID = ID;
}

public String getID()
{
    return ID;
}

public Skill getSkill()
{
    return skill;
}

protected void setSkill(Skill skill)
{
    this.skill = skill;
}

public void setJob(Job job)
{
    this.job = job;
}

public Job getJob()
{
    return job;
}

public void setJobComplete()
{
    job.setStatus(JobStatus.COMPLETE);
}

    public MechanicStatus getStatus() {
        return status;
    }

    
    public void setStatus(MechanicStatus status) {
        this.status = status;
    }
    
    public String toString()
    {
        return "ID: " + ID + " | Name: " + name + " | Rank : " + skill.toString() ;
    }
}


