/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

import java.time.LocalDate;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Alan
 */
public class Job 
{
  private JobStatus status;
  private Customer customer;
  private int number;
  private Date dateCreated;
  private Date dateCompleted;
  private Mechanic mechanic;
  private StockManager stock;
  private JobType jobType;
  
  public Job(Customer customer, StockManager stock, int number)
  {
      this.customer = customer;
      this.stock = stock;
      this.number = number;
      status= JobStatus.PENDING;
      
      dateCreated = new Date();
      
      // Set the type of repair.
      String partType = customer.getRepair().getPartType().getType();
      
      if(partType.equalsIgnoreCase("Tyre"))
      {
          jobType = JobType.TYRE;
      }
      
       if(partType.equalsIgnoreCase("Exhaust"))
      {
          jobType = JobType.EXHAUST;
      }
       
        if(partType.equalsIgnoreCase("engine"))
      {
          jobType = JobType.ENGINE;
      }
      
  }
  
  public Customer getCustomer()
  {
      return customer;
  }
  
  public JobStatus getStatus()
  {
      return status;
  }
  
  public void setStatus(JobStatus status)
  {
      this.status = status;
  }
  
  /**
   * Computes the skill required for the job.
   * @return 
   */
  public Skill skillRequired()
  {
     String skillString = customer.getRepair().getPartType().getType();
     
     if(skillString.equalsIgnoreCase("TYRE"))
     {
         return Skill.TRAINEE;
     }
     
     if(skillString.equalsIgnoreCase("EXHAUST"))
     {
         return Skill.WORKMAN;
     }
     
     if(skillString.equalsIgnoreCase("ENGINE"))
     {
         return Skill.MASTER;
     }
     
     return null;
  }
  
  /**
   * Searched a part by part number.
   * @param partNo
   * @return part object or null.
   */
  public Part findPart(PartType partType, VehicleType vehicleType)
  {
      Set<Part> parts = stock.getParts();
      Iterator<Part> iterator = parts.iterator();
      
      while(iterator.hasNext())
      {
          Part p = iterator.next();
          
          if(p.getPartType() == partType && p.getVehicleType() == vehicleType)
          {
              return p;
          }
      }
      return null;  
  }
  
   public void setMechanic(Mechanic mechanic)
  {
      this.mechanic = mechanic;
  }
  
   public void setDateCompleted(Date date)
   {
       dateCompleted = date;
   }
   
   public Date getDateCompleted()
   {
       return dateCompleted;
   }
   
   public Date getDateCreated()
   {
       return dateCreated;
   }
   
  public int getNumber()
  {
      return number;
  }
  
  public JobType getJobType()
  {
      return jobType;
  }


  @Override
  public String toString()
{

return "Customer: " + customer.getName() + " | Job No: " +number +" | Job type: "+ jobType.name();
}

    /**
     * @return the mechanic
     */
    public Mechanic getMechanic() {
        return mechanic;
    }
    
}
