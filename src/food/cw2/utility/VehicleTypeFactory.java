/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public abstract class VehicleTypeFactory 
{
    /**
     * Produces an instance of a VehicleTpye.
     * @param type determines the instance type returned.
     * @return VehicleType object.
     */
  public static VehicleType getVehicleType(String type)
  {
      if(type.equalsIgnoreCase("ROVER"))
      {
          return new Rover();
      }
      
      if(type.equalsIgnoreCase("FORD"))
      {
          return new Ford();
      }
      
      return null;
  }  
}
