/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public class Report 
{
    private int noOfTyreJob;
    private int noOfExaustJob;
    private int noOfNewEngineJob;
    
    public void incrementNoOfTyreJob()
    {
        noOfTyreJob++;
    }
    
    public int getNoOfTyreJob ()
    {
        return noOfTyreJob;
    }
    
    public void incrementNoOfExaustJob()
    {
        noOfExaustJob++;
    }
    
     public int getNoOfExaustJob ()
    {
        return noOfExaustJob;
    }
     
      public void incrementNoOfNewEngineJob()
    {
        noOfNewEngineJob++;
    }
    
     public int getNoOfNewEngineJob ()
    {
        return noOfNewEngineJob;
    }
    
}
