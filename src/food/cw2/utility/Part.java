/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

import java.math.BigDecimal;

/**
 *
 * @author Alan
 */
public class Part implements Comparable<Part>
{
    private String partNo;
    private VehicleType vehicleType;
    private PartType partType;
    private BigDecimal cost;
    private int quantity;
    private String manufacturer;
    
    public Part(String partNo, VehicleType vehicleType, PartType partType, String manufacturer)
    {
        this.partNo = partNo;
        this.vehicleType = vehicleType;
        this.partType = partType;
        this.manufacturer = manufacturer;
    }
    
    public String getPartNo()
    {
        return partNo;
    }
    
    public VehicleType getVehicleType()
    {
        return vehicleType;
    }
    
    public PartType getPartType()
    {
        return partType;
    }
    
    public void setCost(BigDecimal cost)
    {
        this.cost = cost;
    }
    
    public BigDecimal getCost()
    {
        return cost;
    }
    
    public int getQuantity()
    {
        return quantity;
    }
    
    public void addQuantity(int quantity)
    {
        this.quantity = quantity + this.quantity;
    }
    
    public String getManufacturer()
    {
        return manufacturer;
    }
 
    public void decrementQuantity()
    {
        if(quantity > 0)
        {
        quantity--;
        }
    }

    @Override
    public int compareTo(Part p) {
        return this.partNo.compareTo(p.getPartNo());
    }
    
    public String toString()
    {
        return partNo + " | " + vehicleType.getType() + " | " + partType.getType() + " | " + manufacturer + " | " + quantity;
    }
}
