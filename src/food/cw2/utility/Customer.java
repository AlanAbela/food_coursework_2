/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public class Customer {
    
    private String ID;
    private String name;
    private Repair repair;
    
    public Customer(String name, String ID, Repair repair)
    {
        this.ID = ID;
        this.name = name;
        this.repair = repair;
    }
    
    public String getID()
    {
        return ID;
    }
    
    public Repair getRepair()
    {
        return repair;
    }
    
    public String getName()
    {
        return name;
    }
    
}
