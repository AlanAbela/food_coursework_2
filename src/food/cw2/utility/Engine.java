/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public class Engine implements PartType
{

    @Override
    public String getType() 
    {
        return "Engine";
    }
    
    public String toString()
    {
        return getType();
    }
}
