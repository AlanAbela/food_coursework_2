/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Alan
 */
public class Data 
{
    private StockManager stockManager;
    private List<Job> jobs;
    private List<Mechanic> mechanics;
    private Manager manager;
    private JobNumberGenerator jobNumberGenerator;
            
    public Data()
    {
        jobs = new ArrayList<Job>();
        mechanics = new ArrayList<Mechanic>();
        stockManager = StockManager.getInstance();
        jobNumberGenerator = new JobNumberGenerator(jobs);
        
        ///// Set up a Manager instance //////
        manager = new Manager();
        manager.setJobs(jobs);
        
        ///// Setting up some parts. /////////
        String partNo = "ROVTY";
        VehicleType vehicleType = new Rover();
        PartType partType = PartTypeFactory.getPartType("TYRE");
        String manufacturer = "Pirelli";
        Part part = new Part(partNo, vehicleType, partType, manufacturer);
        part.setCost(new BigDecimal(100));
        part.addQuantity(0);
        stockManager.addPart(part);
        
        partNo = "FORTY";
        vehicleType = new Ford();
        part = new Part(partNo, vehicleType, partType, manufacturer);
        part.setCost(new BigDecimal(110));
        part.addQuantity(10);
        stockManager.addPart(part);
        
        partNo = "ROVEX";
        vehicleType = new Rover();
        partType = PartTypeFactory.getPartType("EXHAUST");
        manufacturer = "Exhusted";
        part = new Part(partNo, vehicleType, partType, manufacturer);
        part.setCost(new BigDecimal(150));
        part.addQuantity(10);
        stockManager.addPart(part);;
        
        partNo = "FOREX";
        vehicleType = new Ford();
        part = new Part(partNo, vehicleType, partType, manufacturer);
        part.setCost(new BigDecimal(170));
        part.addQuantity(10);
        stockManager.addPart(part);
        
         partNo = "ROVEN";
        vehicleType = new Rover();
        partType = PartTypeFactory.getPartType("ENGINE");
        manufacturer = "Delta Engines";
        part = new Part(partNo, vehicleType, partType, manufacturer);
        part.setCost(new BigDecimal(500));
        part.addQuantity(10);
        stockManager.addPart(part);
        
        
        partNo = "FOREN";
        vehicleType = new Ford();
        part = new Part(partNo, vehicleType, partType, manufacturer);
        part.setCost(new BigDecimal(450));
        part.addQuantity(10);
        stockManager.addPart(part);
        
        ////////////////////

        ///// Setup some initial jobs /////
        Repair repair = new Repair(new Rover(), new Tyre());
        Customer customer = new Customer("Alan", "83281M", repair);
        Job job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Rover(), new Exhaust());
        customer = new Customer("Joe", "83282M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Rover(), new Engine());
        customer = new Customer("Mary", "83292M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Ford(), new Tyre());
        customer = new Customer("Mario", "83293M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Ford(), new Exhaust());
        customer = new Customer("Mandy", "83297M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Ford(), new Engine());
        customer = new Customer("Ruben", "83299M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Rover(), new Engine());
        customer = new Customer("Mariano", "83392M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Ford(), new Tyre());
        customer = new Customer("Fantozzi", "83393M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Ford(), new Exhaust());
        customer = new Customer("Keith", "83397M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        repair = new Repair(new Ford(), new Engine());
        customer = new Customer("Jason", "83399M", repair);
        job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
        
        // Setup some mechanics //
        Mechanic m1 = new Trainee("12345", "Tony");
        Mechanic m2 = new Trainee("12346", "Anton");
        Mechanic m3 = new Trainee("12347", "Mark");
        Mechanic m4 = new Workman("12348", "Ryan");
        Mechanic m5 = new Workman("12349", "Duane");
        Mechanic m6 = new Workman("123410", "Daniel");
        Mechanic m7 = new Master("123411", "Joe");
        Mechanic m8 = new Master("123412", "Justin");
        Mechanic m9 = new Master("123413", "Steven");
        
        mechanics.add(m1);
        mechanics.add(m2);
        mechanics.add(m3);
        mechanics.add(m4);
        mechanics.add(m5);
        mechanics.add(m6);
        mechanics.add(m7);
        mechanics.add(m8);
        mechanics.add(m9);
        
        ////// Pass mechanics to the manager instance //////
        manager.setMechanics(mechanics);
    }
    
    public StockManager getStockManager()
    {
        return stockManager;
    }
    
    public List<Job> getJobs()
    {
        return jobs;
    }
    
    /**
     * Retrieves a job object.
     * @param jobNo
     * @return Job or null.
     */
    public Job getJob(int jobNo)
    {
        for(Job j : jobs)
        {
            if(j.getNumber() == jobNo)
            {
                return j;
            }
        }
        return null;
    }
    
    public void addJob(String customerName, String customerID, VehicleType vehicleType, PartType repairType)
    {
        Repair repair = new Repair(vehicleType, repairType);
        Customer customer = new Customer(customerName, customerID, repair);
        Job job = new Job(customer, stockManager, jobNumberGenerator.getNextNumber(jobs));
        jobs.add(job);
    }
    
    /**
     * Gets the cost of the part.
     * @param vehicleType
     * @param partType
     * @return BigDecimal object or null.
     */
    public BigDecimal getCost(String vehicleType, String partType)
    {   
        Set<Part> parts = stockManager.getParts();
        BigDecimal cost = null;
        Iterator<Part> iterator = parts.iterator();
        
        while(iterator.hasNext())
        {
            Part p = iterator.next();
           if(p.getVehicleType().getType().equals(vehicleType) && p.getPartType().getType().equals(partType))
           {
               cost = p.getCost();
           }
        }
        return cost;
    }
    
    public List<Mechanic> getAvailableMechanics()
    {
        List<Mechanic> list = new ArrayList();
        
        for(Mechanic m : mechanics)
        {
            if(m.getStatus() == MechanicStatus.IDLE)
            {
                list.add(m);
            }
        }
        
        return list;
    }
    
    public void takePart(String partType, String vehicleType)
    {
        stockManager.takeItem(partType, vehicleType);
    }
    
    public void setJobOngoing(int jobNo)
    {
        for(int i = 0; i<jobs.size(); i++)
        {
            if(jobs.get(i).getNumber() == jobNo)
            {
                jobs.get(i).setStatus(JobStatus.ONGOING);
              
            }
        }
    }
    
    public Mechanic getMechanicByID(String ID)
    {
        for(int i = 0; i<mechanics.size(); i++)
        {
            if(mechanics.get(i).getID().equalsIgnoreCase(ID))
                return mechanics.get(i);
        }
        return null;
    }
    
    public List<Job> getOngoingJobs()
    {
        List<Job> list = new ArrayList();
        
       for(int i = 0; i<jobs.size(); i++)
       {
           if(jobs.get(i).getStatus() == JobStatus.ONGOING)
           {
               list.add(jobs.get(i));
           }
       }
       
       return list;
    }
    
    
    public void setComplete(int jobNo)
    {
        for(int i = 0; i<jobs.size(); i++)
        {
            if(jobs.get(i).getNumber() == jobNo)
            {
                jobs.get(i).setStatus(JobStatus.COMPLETE);
                return;
            }
        }
    }
    
    public Manager getManager()
    {
        return manager;
    }
    
    /**
     * Retrieves promotable mechanics.
     * @return 
     */
    public List<Promotable> getPromotable()
    {
        List<Promotable> list = new ArrayList();
        
        for(Mechanic m : mechanics)
        {
            if(m instanceof Promotable)
            {
                list.add((Promotable)m);
            }
        }
        
        return list;
    }
    
    public List<PartType> getPartTypes()
    {
        List<PartType> list = new ArrayList<PartType>();
        list.add(PartTypeFactory.getPartType("TYRE"));
        list.add(PartTypeFactory.getPartType("EXHAUST"));
        list.add(PartTypeFactory.getPartType("ENGINE"));
        
        return list;

    }
        
     public JobNumberGenerator GetJobNumberGenerator()
    {
        return jobNumberGenerator;
    }
     
     // Retrieves a complete job by job number.
    public Job getCompletedJob(int jobNo)
    {
        for(Job j : jobs)
        {
            if(j.getStatus() == JobStatus.COMPLETE && j.getNumber() == jobNo)
            {
                return j;
            }
        }
        return null;
    }
}
