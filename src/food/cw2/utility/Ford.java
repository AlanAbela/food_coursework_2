/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

/**
 *
 * @author Alan
 */
public class Ford implements VehicleType{

    @Override
    public String getType() {
        return "Ford";   
    }
    
    public String toString()
    {
        return getType();
    }
    
}
