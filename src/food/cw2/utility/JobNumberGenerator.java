/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package food.cw2.utility;

import java.util.List;

/**
 *
 * @author Alan
 */
public class JobNumberGenerator {
    
    private  int lastNumber = 0;
    private List<Job> jobs;
    
    public JobNumberGenerator(List<Job> jobs)
    {
        this.jobs = jobs;
    }
    
    public int getNextNumber(List<Job> jobs)
    {
        // If there is only one instance add 1 and return the value.
        if(jobs.size() == 1)
        {
            return jobs.get(0).getNumber()+1;
        }
        
        // If List is larger than 1, compare and return the largest value plus 1.
      for(int i =0; i<jobs.size(); i++)
      {
          for(int j = 0; j<jobs.size(); j++)
          {
             if(jobs.get(i).getNumber() > jobs.get(j).getNumber())
             {
                 lastNumber = jobs.get(i).getNumber();
             }
             else if(jobs.get(i).getNumber() < jobs.get(j).getNumber())
             {
                 lastNumber = jobs.get(j).getNumber();
             }
          }
      }    
      return lastNumber+1;
    }
    
}
